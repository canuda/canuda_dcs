# Parameter definitions for thorn dCS_Init
#===============================================

#=== pars from/for ADMBase ==================
SHARES: ADMBase

#=== pars from dCS_Base ================
SHARES: dCS_Base

## HW: shorten to include only initial data that is available
EXTENDS KEYWORD dCS_initdata "Which scalar field initial data"
{
  "ID_dCS_Gaussian"                  :: "use Gaussian initial data"
  "ID_dCS_Zero"                      :: "initialize zero scalar field"
  "ID_dCS_SBH_SlowRot"               :: "slow rotation approximation for scalar field"
  "ID_dCS_BS"                        :: "scalar field bound state"
  "ID_dCS_SBH_SlowRot_plus_Gaussian" :: "dCS hair solution plus gaussian perturbation"
}

USES CCTK_REAL LS
USES CCTK_REAL aHat
USES CCTK_REAL mu_theta
USES CCTK_REAL fTheta_lambda

#=== local parameters ==========================
RESTRICTED:

#===============================================
#=== parameters for BH binary ==================
#===============================================

CCTK_REAL m_plus "ADM mass for m+"
{
  0.0:* :: "should be the same as in TwoPunctures"
} 0.5

CCTK_REAL m_minus "ADM mass for m-"
{
  0.0:* :: "should be the same as in TwoPunctures"
} 0.5

CCTK_REAL pos_plus[3] "position of the m+ puncture"
{
  *:* :: "any value possible"
} 0.0

CCTK_REAL pos_minus[3] "position of the m- puncture"
{
  *:* :: "any value possible"
} 0.0

CCTK_REAL spin_plus "spin of the 1st Kerr BH with z being the rotation axis"
{
  0:1  :: "any value between 0 and 1"
} 0.0

CCTK_REAL spin_minus "spin of the 2nd Kerr BH with z being the rotation axis"
{
  0:1  :: "any value between 0 and 1"
} 0.0

#===============================================
#=== parameters for Gaussian initial data ======
#===============================================

KEYWORD dCS_GaussType "which angular distribution for Gaussinan DF initial data?"
{
  "SF_Gauss00" :: "Z=Y00"
  "SF_Gauss10" :: "Z=Y10"
  "SF_Gauss11" :: "Z=Y1m1-Y11"
  "SF_Gauss22" :: "Z=Y22+Y2m2+Y20"
  "SF_Gauss0011" :: "Z=Y00+Y1m1-Y11"
} "SF_Gauss00"

CCTK_REAL ampSF "amplitude of scalar field"
{
  0:*  :: "any positive value possible"
} 1.0

CCTK_REAL r0 "scalar field parameter"
{
  0:*  :: "any positive value possible"
} 1.0

CCTK_REAL width "scalar field width"
{
  0:*  :: "any positive value possible"
} 1.0

#=========================================
#=== parameters for BS initial data ======
#=========================================

CCTK_REAL wR "real part of frequency for bound state"
{
  *:*  :: "depends on spin and mass; 0.408806 for l=m=1, a/M=0.99, mu=0.42"
} 0.0

CCTK_REAL wI "im part of frequency for bound state"
{
  *:*  :: "depends on spin and mass; 1.50391*10^{-7} for l=m=1, a/M=0.99, mu=0.42"
} 0.0

CCTK_REAL A0  "zeroth element in Sum, Eq.33 of Dolan '07"
{
  0:*  :: "..."
} 1.0

CCTK_REAL Ap0  "zeroth element in Sum for spheroidal harmonics"
{
  0:*  :: "..."
} 1.0

CCTK_INT n_sum "numbers of elements in Sum, Eq.33 Dolan '07"
{
  1:*  :: "..."
} 200

#=======================================================
#==== parameters for constant nonzero initial data =====
#=======================================================
## HW: comment out because it is not currently used
##
#CCTK_REAL ampConstSF "amplitude of nonzero scalar field"
#{
#  0:*  :: "any positive value possible"
#} 1.0d-02

#=======================================================
#==== parameters for BHB scalar field inital data  =====
#=======================================================
## HW: comment out because it is not currently used
##
#CCTK_REAL ampsf_m_plus "scalar field amplitude for m+"
#{
#  0:* :: "if zero the scalar field is initially bald"
#} 1.0
#
#CCTK_REAL ampsf_m_minus "scalar field amplitude for m-"
#{
#  0:* :: "if zero the scalar field is initially bald"
#} 1.0

#===============================================
#=== auxiliary parameters =====================
#===============================================

CCTK_REAL eps_r "of value if r approaches zero"
{
  0:*   :: "any small positive value possible"
} 1.0d-06

CCTK_REAL eps_r2 "of value if r2 approaches zero"
{
  0:*   :: "any small positive value possible"
} 1.0d-10

BOOLEAN schedule_in_ADMBase_InitialData "Schedule in (instead of after) ADMBase_InitialData"
{
} "yes"
