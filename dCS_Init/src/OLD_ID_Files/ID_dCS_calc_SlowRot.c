/* intial data thorn: ID_dCS_SlowRot */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_dCS_utils.h"

#define PI 3.141592653589793238462643383279502884197169399375

/* -------------------------------------------------------------------*/
void ID_dCS_SlowRot(CCTK_ARGUMENTS);
void
ID_dCS_SlowRot(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin dCS initial data ===");

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL LS2;
  LS2 = LS * LS;
  CCTK_REAL mp, mp2, mp3, mp4, mp5, mp6, mp7;
  CCTK_REAL ap;
  mp = m_plus;
  mp2 = mp * mp;
  mp3 = mp2 * mp;
  mp4 = mp3 * mp;
  mp5 = mp4 * mp;
  mp6 = mp5 * mp;
  mp7 = mp6 * mp;
  ap = spin_plus;
  /*----------------------------*/

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    /*----------------------------------*/
    CCTK_REAL xp[3];
    CCTK_REAL rrp, rrp2, rrp3, rrp4, rrp5, rrp6, rrp7;
    CCTK_REAL CosThp;

    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    rrp = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;
    rrp3 = rrp2 * rrp;
    rrp4 = rrp3 * rrp;
    rrp5 = rrp4 * rrp;
    rrp6 = rrp5 * rrp;
    rrp7 = rrp6 * rrp;
    CosThp = xp[3]/rrp;

    /*----------------------------------*/
    /*=== define scalar fields =========*/
    /*------------------------------------*/
    // The scalar field profile is that of a single slowly rotating  
    // BH in dCS, i.e. see eq (22) in 1206.6130
    CCTK_REAL theta_plus;

    if( mp == 0 )
    {
      theta_plus = 0;
    } else
    {
      theta_plus = 5.0/8.0 * LS2 * (ap/mp) * (CosThp/rrp2) * (1.0 + 2.0*mp/rrp
                   + 18.0*mp2/(5.0*rrp2))
    }

    /*------------------------------------*/

    /*=== define scalar field momentum ===*/
    /*------------------------------------*/

     /*=== write to grid functions ============================*/
     Theta_gf[ind] = theta_plus;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End dCS initial data ===");

}
