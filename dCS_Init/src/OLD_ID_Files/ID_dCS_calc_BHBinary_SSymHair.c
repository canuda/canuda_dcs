/* intial data thorn: ID_dCS_BHBinary_SSymHair */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_dCS_utils.h"

/* -------------------------------------------------------------------*/
void ID_dCS_BHBinary_SSymHair(CCTK_ARGUMENTS);
void
ID_dCS_BHBinary_SSymHair(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin dCS initial data ===");

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL mp, mm;
  mp = m_plus;
  mm = m_minus;

  /*----------------------------*/

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);


    /*=== define position parameters ===*/
    /*----------------------------------*/
    CCTK_REAL xp[3], xm[3];
    CCTK_REAL rrp, rrp2, rrp3, rrp4;
    CCTK_REAL rrm, rrm2, rrm3, rrm4;

    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    xm[0] = x[ind] - pos_minus[0];
    xm[1] = y[ind] - pos_minus[1];
    xm[2] = z[ind] - pos_minus[2];

    rrp = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;
    rrp3 = rrp2 * rrp;
    rrp4 = rrp2 * rrp2;

    rrm = sqrt( xm[0] * xm[0] + xm[1] * xm[1] + xm[2] * xm[2] );
    if( rrm < eps_r ) rrm = eps_r;
    rrm2 = rrm  * rrm;
    rrm3 = rrm2 * rrm;
    rrm4 = rrm2 * rrm2;

    /*----------------------------------*/

    /*=== define scalar fields =========*/
    /*------------------------------------*/
    // The individual scalar field profiles are those for single, non-rotating hairy BH solutions
    // in shift-symmetric dCS; see e.g. ...
    CCTK_REAL theta_plus, theta_minus;
    CCTK_REAL thetaBinary;

    if( mp == 0 )
    {
      theta_plus = 0;
    } else
    {
      theta_plus = 2.0 * pow(RL, 2) * rrp / pow( mp + 2.0 * rrp, 6)
                     * ( mp*mp*mp + 12.0*mp*mp * rrp + 184.0*mp * rrp2 / 3 + 48.0 * rrp3 + 16.0 * rrp4 / mp );
    }

    if( mm == 0 )
    {
      theta_minus = 0;
    } else
    {
      theta_minus = 2.0 * pow(RL, 2) * rrm / pow( mm + 2.0*rrm , 6 )
                      * ( mm*mm*mm + 12.0*mm*mm * rrm + 184.0*mm * rrm2 / 3 + 48.0 * rrm3 + 16.0 * rrm4 / mm );
    }


    // Approximate binary solution by simply superposing the individual
    // scalar fields solutions
    thetaBinary = theta_plus + theta_minus;

    /*------------------------------------*/

    /*=== define scalar field momentum ===*/
    /*------------------------------------*/

     /*=== write to grid functions ============================*/
     Theta_gf[ind]  = thetaBinary;
     KTheta_gf[ind] = 0.0;

     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End dCS initial data ===");

}
