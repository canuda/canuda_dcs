/* intial data thorn: ID_dCS_Gauss */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_dCS_utils.h"

/* -------------------------------------------------------------------*/
void ID_dCS_Gauss(CCTK_ARGUMENTS);
void
ID_dCS_Gauss (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_dCS_Gauss initial data ===");

  /*=== define parameters ====================================*/
  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  /*----------------------------------------------------------*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Theta_gf[ind]  = 0.0;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
     CCTK_REAL theta, Ktheta;
     theta  = 0.0;
     Ktheta = 0.0;
     CCTK_REAL CoefAng;
     CoefAng = 0.0;
     /*========================================================*/

     /*=== define radius and angles ===========================*/
     // positions
     // NOTE: the Gaussian shell is anchored around the origin
     CCTK_REAL xp[3];
     xp[0] = x[ind];
     xp[1] = y[ind];
     xp[2] = z[ind];

     // coordinate radius and polar radial coordinate
     CCTK_REAL rr, rr2;
     rr = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rr < eps_r ) rr = eps_r;
     rr2 = rr*rr;

     // angles
     /*========================================================*/

     /*=== initialize spherical harmonics =====================*/
     if( CCTK_EQUALS( dCS_GaussType, "SF_Gauss00" ) )
     // Z=Y00
     {
       CoefAng = 1.0 / sqrt( 4.0*Pi );
     }
     else if( CCTK_EQUALS( dCS_GaussType, "SF_Gauss10" ) )
     // Z=Y10
     {
       CoefAng = sqrt( 0.75 / Pi ) * xp[2] / rr;
     }
     else if( CCTK_EQUALS( dCS_GaussType, "SF_Gauss11" ) )
     // Z=Y1m1-Y11
     {
       CoefAng = sqrt( 1.5 / Pi ) * xp[0] / rr;
     }
     else if( CCTK_EQUALS( dCS_GaussType, "SF_Gauss22" ) )
     // Z=Y22+Y2m2+Y20
     {
       CoefAng = sqrt( 5.0 / (16*Pi) ) * ( 2.0 + ( sqrt(6.0) - 3 )*xp[0]*xp[0] / rr2 - ( sqrt(6.0) + 3 )*xp[1]*xp[1] / rr2 );
     }
     else if( CCTK_EQUALS( dCS_GaussType, "SF_Gauss0011" ) )
     // Z = Y00 + Y1m1-Y11
     {
       CoefAng = 1.0 / sqrt( 4.0*Pi )
                 + sqrt( 1.5 / Pi ) * xp[0] / rr;

     }
     /*-----------------------------------------------------*/
     else
     CCTK_WARN (0, "invalid dCS field initial data");
     /*-----------------------------------------------------*/

     /* -----------------------------------------------------------------------
      * We assume that Ktheta = 0 and let the code adjust itself. The correct
      * approach would be to compute
      *
      * Ktheta = - {\cal L}_n \Theta
      *      = (1/alpha) * (\partial_t - {\cal L}_{\beta}) \Theta
      *      ~ (1/alpha) * \beta^{k} \partial_{k} \Theta
      *
      * with lapse (alpha) and shift (beta) given by the background spacetime.
      * -------------------------------------------------------------------- */

    theta  = ampSF * CoefAng * exp( -( rr - r0 )*( rr - r0 ) / ( width*width ) );
    Ktheta = 0.0;

    /*========================================================*/

    /*=== write to grid functions ============================*/
    Theta_gf[ind]  = theta;
    KTheta_gf[ind] = Ktheta;

    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*=== end of loops over grid =================================*/
  /*============================================================*/

  CCTK_INFO("=== End ID_dCS_Gauss initial data ===");

}
/* -------------------------------------------------------------------*/
