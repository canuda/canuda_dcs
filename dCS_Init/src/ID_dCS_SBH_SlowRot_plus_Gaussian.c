//* intial data thorn: ID_dCS_SBH_SlowRot_plus_Gaussian */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_dCS_utils.h"

/* -------------------------------------------------------------------*/
void ID_dCS_SBH_SlowRot_plus_Gaussian(CCTK_ARGUMENTS);
void
ID_dCS_SBH_SlowRot_plus_Gaussian (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_dCS_SBH_SlowRot_plus_Gaussian initial data ===");

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL mp, mp2, mp3, mp4, mp5;
  mp = m_plus;
  mp2 = mp * mp;
  mp3 = mp2 * mp;
  mp4 = mp2 * mp2;
  mp5 = mp4 * mp;
  CCTK_REAL chip, chip2, chip3, chip4;
  chip = spin_plus/m_plus;
  chip2 = chip * chip;
  chip3 = chip2 * chip;
  chip4 = chip2 * chip2;
  CCTK_REAL rplus;
  rplus = mp + sqrt(mp*mp - spin_plus*spin_plus); //BHp horizon
  CCTK_REAL xp[3];
  CCTK_REAL rrp, rrp2, rrp3, rrp4, rrp5, rrp6, rrp7;
  CCTK_REAL CosThp, CosThp2, CosThp3, CosThp4;
  CCTK_REAL rqip;

  // def LS^2
  CCTK_REAL LS2;
  LS2 = LS*LS;

  /*=== define scalar fields =========*/
  /*------------------------------------*/
  // The individual scalar field profiles are those for single,
  // slowly-rotating BHs in axi-dilaton gravity described in
  // https://arxiv.org/abs/2111.04750
  // Here we implement the solution for the axion, ie theta
  CCTK_REAL theta_plus;

  /*=== define variable for Gaussian ID ====*/
  /*------------------------------------*/
  // Define Gaussian perturbation for l = m = 1 modes to test energy transfer and decay rates.
  CCTK_REAL theta_Gauss;
  CCTK_REAL CoefAng;

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);
    
    /*=== initialize grid functions as zero ==================*/
     Theta_gf[ind]  = 0.0;
    /*========================================================*/

  /*=== initialize local functions as zero =================*/

      theta_Gauss  = 0.0;

      CoefAng = 0.0;
  /*========================================================*/

    /*=== define position parameters ===*/
    /*----------------------------------*/
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    CosThp = xp[2]/rqip;
    CosThp2 = CosThp * CosThp;
    CosThp3 = CosThp2 * CosThp;
    CosThp4 = CosThp2 * CosThp2;
    rrp = rqip * pow(1.0 + rplus / (4.0 * rqip),2); //polar radius from BHp
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;
    rrp3 = rrp2 * rrp;
    rrp4 = rrp3 * rrp;
    rrp5 = rrp4 * rrp;
    rrp6 = rrp5 * rrp;
    rrp7 = rrp6 * rrp;

    /*----------------------------------*/
   // This is - the solution provided in Cano et al.
    theta_plus = -2.0 * aHat*LS2*fTheta_lambda * ( + chip * (- CosThp * ( 5.0/(16.0*rrp2) + 5.0*mp/(8.0*rrp3) + 9.0*mp2/(8.0*rrp4) ))
      + chip3 * (+ CosThp * ( 1.0/(32.0*rrp2) + mp/(16.0*rrp3) + 3.0*mp2/(40.0*rrp4) + mp3/(20.0*rrp5) )
      + CosThp3 * ( 3.0*mp2/(8.0*rrp4) + 3.0*mp3/(2.0*rrp5) + 25.0*mp4/(6.0*rrp6) ) )
     );


    /*=== initialize spherical harmonics =====================*/
	
    // Z = Y1m1-Y11 

    CoefAng = sqrt( 1.5 / Pi ) * xp[0] / rqip;    

    /*=================== set Gaussians  =====================*/

     theta_Gauss = ampSF * CoefAng * exp( -( rqip - r0 )*( rqip - r0 ) / ( width*width ) );


    /*=== write to grid functions ============================*/
    Theta_gf[ind] = theta_plus + theta_Gauss;
    KTheta_gf[ind] = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End ID_dCS_SBH_SlowRot_plus_Gaussian initial data ===");

}
