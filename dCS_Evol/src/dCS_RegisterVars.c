
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void dCS_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT ierr = 0, group, rhs;

  // register evolution and rhs gridfunction groups with MoL

  /* ADM metric and extrinsic curvature */
  group = CCTK_GroupIndex("ADMBase::lapse");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::shift");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::metric");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::curv");
  ierr += MoLRegisterSaveAndRestoreGroup(group);

  /* Scalar field */
  group = CCTK_GroupIndex("dCS_Base::theta_group");
  rhs   = CCTK_GroupIndex("dCS_Evol::rhs_theta_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

  /* Scalar field momentum */
  group = CCTK_GroupIndex("dCS_Base::Ktheta_group");
  rhs   = CCTK_GroupIndex("dCS_Evol::rhs_Ktheta_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

  if (ierr) CCTK_ERROR("Problems registering with MoL");

}
