! dCS_Boundaries.F90
!
!=============================================================================

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

subroutine dCS_Boundaries( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS

  CCTK_INT ierr

  CCTK_INT, parameter :: one = 1


  ! The outgoing (radiative) boundary conditions are being handled from the rhs
  ! routine through calls to the NewRad infrastructure. Here we register all
  ! BCs as 'none', which enforces all the symmetry BCs.

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Base::theta_group", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Base::theta_group!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Base::Ktheta_group", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Base::Ktheta_group!")


end subroutine dCS_Boundaries
!
!=============================================================================
!
subroutine dCS_WBSSN_Boundaries( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS

  CCTK_INT ierr

  CCTK_INT, parameter :: one = 1


  ! The outgoing (radiative) boundary conditions are being handled from the rhs
  ! routine through calls to the NewRad infrastructure. Here we register all
  ! BCs as 'none', which enforces all the symmetry BCs.

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::conf_fac_WW", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::conf_fac_WW!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::hmetric", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::hmetric!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::hcurv", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::hcurv!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::trk", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::trk!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::gammat", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::gammat!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "ADMBase::lapse", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for ADMBase::lapse!")

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "ADMBase::shift", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for ADMBase::shift!")


end subroutine dCS_WBSSN_Boundaries
!
!=============================================================================
!
subroutine dCS_PontDen_Boundaries( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS
  DECLARE_CCTK_FUNCTIONS

  CCTK_INT ierr

  CCTK_INT, parameter :: one = 1

  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
       "dCS_Evol::dCS_Invariant", "none")
  if (ierr < 0)                                                            &
       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::dCS_Invariant!")


end subroutine dCS_PontDen_Boundaries
!
!=============================================================================
!
subroutine dCS_calc_TmnEff_boundaries( CCTK_ARGUMENTS )

   implicit none
   DECLARE_CCTK_ARGUMENTS
   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS

   CCTK_INT ierr

   CCTK_INT, parameter :: one = 1

   ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
         "dCS_Evol::dCS_TmnEff_Sca_gfs", "none")
   if (ierr < 0)                                                            &
        call CCTK_WARN(0, "Failed to register BC for dCS_Evol::dCS_TmnEff_Sca_gfs")

   ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
         "dCS_Evol::dCS_TmnEff_Vec_gfs", "none")
   if (ierr < 0)                                                            &
        call CCTK_WARN(0, "Failed to register BC for dCS_Evol::dCS_TmnEff_Vec_gfs")

   ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
         "dCS_Evol::dCS_TmnEff_Ten_gfs", "none")
   if (ierr < 0)                                                            &
        call CCTK_WARN(0, "Failed to register BC for dCS_Evol::dCS_TmnEff_Ten_gfs")

end subroutine dCS_calc_TmnEff_boundaries
!
!=============================================================================
!
!! HW: take off old routine
!! subroutine dCS_densities_boundaries( CCTK_ARGUMENTS )
!! 
!!   implicit none
!!   DECLARE_CCTK_ARGUMENTS
!!   DECLARE_CCTK_PARAMETERS
!!   DECLARE_CCTK_FUNCTIONS
!! 
!!   CCTK_INT ierr
!! 
!!   CCTK_INT, parameter :: one = 1
!! 
!!   ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
!!         "dCS_Evol::densities_dCS", "flat")
!!   if (ierr < 0)                                                            &
!!        call CCTK_WARN(0, "Failed to register BC for dCS_Evol::densities_dCS")
!! 
!! 
!! end subroutine dCS_densities_boundaries
!
!=============================================================================
!
! HW: TODO: take this off
!subroutine dCS_norms_boundaries( CCTK_ARGUMENTS )
!
!  implicit none
!  DECLARE_CCTK_ARGUMENTS
!  DECLARE_CCTK_PARAMETERS
!  DECLARE_CCTK_FUNCTIONS
!
!  CCTK_INT ierr
!
!  CCTK_INT, parameter :: one = 1
!
!  ierr = Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, one, -one,      &
!        "dCS_Evol::dCS_norm_gfs", "flat")
!  if (ierr < 0)                                                            &
!       call CCTK_WARN(0, "Failed to register BC for dCS_Evol::dCS_norm_gfs")
!
!
!end subroutine dCS_norms_boundaries
!
!=============================================================================
!
