
# Canuda_dCS

# Author:     Chloe Richards, Alexandru Dima, Helvi Witek
# Developer:  Chloe Richards, Alexandru Dima, Helvi Witek
# Maintainer: Chloe Richards, Alexandru Dima, Helvi Witek

# Purpose:
Code capable to simulate black holes in dynamical Chern-Simons gravity
in the decoupling limit.
Compatible with the Einstein Toolkit.

# Projects:
Simulate the growth of scalar hair in the presence of a massive scalar field
in dynamical Chern-Simons (dCS) gravity. 
    - See: arxiv:2304.XXXXX
    - Animations: https://www.youtube.com/@canudanumericalrelativity1634
    - For example parameter files to perform these simulations, please see 
      the folder Cactus/repos/Canuda_dCS/par

# Parameter choices:

See ../dCS_Base/param.ccl for more details
aHat: "switch" to coupling to Pontryagin density
    - aHat = 0: GR
    - aHat = 1: dCS gravity

See ../dCS_Init/param.ccl for more details
dCS_coupling: choice of scalar coupling to the Pontryagin density
    - dCS_coupling = shiftsymmetric: typical choice

dCS_potential: potential of the scalar field
    - dCS_potential = zero: typical choice dCS simulations
    - dCS_potential = mass: see arxiv:2304.XXXXX
