# $Header:$

shares: IO
USES KEYWORD  recover

restricted:

KEYWORD evolution_method "evolution method for scalar fields"
{
  "none" :: "scalar field is not evolved"
} "none"

KEYWORD dCS_initdata "initial data for scalar field"
{
  "none" :: "scalar field variables are not initialized"
} "none"

# Chern-Simons reference scale
# ====================================================
## Relation between coupling parameter aCS and reference length is $aCS = aHat*LS^2$
## Set LS = 1M as characteristic length scale.
## Should have LS = (total mass of BH) for consistency.
CCTK_REAL LS "Chern-Simons reference scale"
{
  0:*   :: "any positive value allowed"
} 1.0

# Coupling to Pontryagin density: switch on (aHat = 1), i.e. dCS, or off (aHat = 0), i.e. GR
# NOTE: aHat defined to take any value, but really aHat = {-1, 0, 1} 
# Default case gives GR
CCTK_REAL aHat "switch parameter to turn on/off coupling to Pontryagin density"
{
  *:*   :: "Any value allowed"
} 0.0

CCTK_REAL f_axi "axion coupling"
{
  *:* :: "Any value possible"
} 1.0

# Parameters for scalar field coupling function
#=====================================================================

KEYWORD dCS_coupling "type of coupling function"
{
  "shiftsymmetric" :: "shift-symmetric coupling f=Theta"
  "cubic"          :: "cubic coupling function f=Theta3"
  "dilaton"        :: "dilaton coupling f=exp(Theta) "
} "shiftsymmetric"

CCTK_REAL fTheta_lambda "Coefficient in exponential coupling function"
{
  *:* :: "Any value possible"
} 1.0

KEYWORD dCS_potential "dCS potential"
{
  "zero" :: "zero potential"
  "mass" :: "mass potential"
  "periodic" :: "periodic potential"
} "zero"

CCTK_REAL mu_theta "mass of scalar field theta" 
{
  *:* :: "Any value possible"
} 0.0

# Parameters for boundary conditions
# ===================================

CCTK_INT n_Theta "n power of outgoing boundary r^n fall off rate for Theta"
{
  0:4           :: "should be 1/r"
} 1

CCTK_INT n_KTheta "n power of outgoing boundary r^n fall off rate for KTheta"
{
  0:4           :: "should be DTheta sim 1 / r^2"
} 2

CCTK_REAL Theta0 "asymptotic value for Theta"
{
  *:*           :: ""
} 0

CCTK_REAL KTheta0 "asymptotic value for KTheta"
{
  *:*           :: ""
} 0

